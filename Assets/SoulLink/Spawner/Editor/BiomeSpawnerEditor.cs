using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    [CustomEditor(typeof(BiomeSpawner), true)]
    public class BiomeSpawnerEditor : Editor
    {
        private BiomeSpawner _myTarget;
        private SerializedObject _mySerializedObject;

        // bool

        // float

        // int

        // string

        // enum

        // object
        SerializedProperty DatabaseProp, PlayerTransformProp;

        private void OnEnable()
        {
            _myTarget = (BiomeSpawner)target;
            _mySerializedObject = new SerializedObject(_myTarget);

            // object property initializers
            DatabaseProp = _mySerializedObject.FindProperty("_SoulLinkSpawnerDatabase");
            PlayerTransformProp = _mySerializedObject.FindProperty("_playerTransform");
        } // OnEnable()

        public override void OnInspectorGUI()
        {
            EditorUtils.Init();

            _mySerializedObject.Update();

            EditorUtils.BeginBgndStyle();

            DrawRuntimeDataGUI();

            EditorGUILayout.EndVertical();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(_myTarget);
                _mySerializedObject.ApplyModifiedProperties();
            }
        } // OnInspectorGUI()

        void DrawRuntimeDataGUI()
        {
            GUI.enabled = false;
            EditorGUILayout.TextField("Database", _myTarget.Database != null ? _myTarget.Database.name : "");
            EditorGUILayout.TextField("Player", _myTarget.PlayerTransform != null ? _myTarget.PlayerTransform.name : "");

            int totalSpawns = 0;
            EditorGUI.indentLevel++;
            foreach (var population in _myTarget.Populations)
            {
                EditorGUILayout.TextField(population.Key.name, population.Value.ToString());

                totalSpawns += population.Value;
            } // foreach
            EditorGUI.indentLevel--;
            EditorGUILayout.TextField("Total Spawns", totalSpawns.ToString());
            GUI.enabled = true;

            Repaint();
        } // DrawRuntimeDataGUI()
    } // class BiomeSpawnerEditor
} // namespace Magique.SoulLink

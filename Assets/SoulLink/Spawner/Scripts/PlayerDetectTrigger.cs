using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A class for detecting when a player enters or exits a spawn's spawning range
// This is used for adding to and subtracting from a player's spawn populations.
//
// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    public class PlayerDetectTrigger : MonoBehaviour
    {
        public List<Transform> PlayersTriggered
        { get {  return _playersTriggered; } }

        private ISpawnable _spawnable;
        private List<Transform> _playersTriggered = new List<Transform>(); // the list of players currently triggered by this spawnable
        private float _despawnRange = 0f;
        private bool _enabled = false;

        private void Awake()
        {
            _spawnable = GetComponentInParent<ISpawnable>();
            _despawnRange = SoulLinkSpawner.Instance.SpawnRadius + _spawnable.GetDespawnRangeBuffer();
        } // Awake()

        /// <summary>
        /// Enable/Disable the trigger by enabling/disabling its collder
        /// </summary>
        /// <param name="enabled"></param>
        public void Enable(bool enabled = true)
        {
            if (_enabled == enabled) return;
            _enabled = enabled;

            if (enabled)
            {
                _playersTriggered.Clear();
            } // if
        } // Enable()

        private void FixedUpdate()
        {
            if (!_enabled) return;

            foreach (var player in SoulLinkSpawner.Instance.Players)
            {
                if (player.Key == null) continue;

                // If player is in range then add to _playersTriggered and add to its population
                if (Vector3.Distance(player.Key.position, transform.position) <= SoulLinkSpawner.Instance.SpawnRadius)
                {
                    if (!_playersTriggered.Contains(player.Key))
                    {
                        _playersTriggered.Add(player.Key);
                        SoulLinkSpawner.Instance.PopulationAdd(player.Key, _spawnable.GetSpawnPrefab(), _spawnable.GetSpawnCategory());
                    } // if
                } // if

                // If player is out of range and in _playersTriggered then remove from list and subtract from population
                if (_playersTriggered.Contains(player.Key))
                {
                    if (Vector3.Distance(player.Key.position, transform.position) > _despawnRange)
                    {
                        _playersTriggered.Remove(player.Key);

                        SoulLinkSpawner.Instance.PopulationSubtract(player.Key, _spawnable.GetSpawnPrefab(), _spawnable.GetSpawnCategory());
                        if (player.Key == _spawnable.GetPlayer())
                        {
                            SoulLinkSpawner.Instance.ReturnRandomChance(player.Key, _spawnable.GetRandomChance());
                            _spawnable.SetRandomChance(0); // set to 0 so it cannot be returned more than once
                        } // if

                        // if no players have this spawn triggered then despawn it
                        if (_playersTriggered.Count == 0)
                        {
                            SoulLinkGlobal.DebugLog(gameObject, "Despawning");
                            SoulLinkSpawner.Instance.Despawn(_spawnable.GetGameObject().transform);
                        } // if
                    } // if
                } // if
            } // foreach
        } // FixedUpdate()
    } // class PlayerDetectTrigger
} // namespace Magique.SoulLink

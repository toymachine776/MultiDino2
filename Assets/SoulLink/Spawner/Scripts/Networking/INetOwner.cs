
// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    public interface INetOwner
    {
        void SetIsOwner(bool isOwner);

        bool IsOwner();
    } // interface IOwner
} // namespace Magique.SoulLink
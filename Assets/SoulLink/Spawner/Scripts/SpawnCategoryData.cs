namespace Magique.SoulLink
{
    [System.Serializable]
    public class SpawnCategoryData
    {
        public string categoryName;
        public int maxSpawns = 50;
    } // class SpawnCategoryData
} // namespace Magique.SoulLink

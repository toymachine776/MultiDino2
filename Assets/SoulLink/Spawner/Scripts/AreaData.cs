namespace Magique.SoulLink
{
    [System.Serializable]
    public class AreaData
    {
        public string areaName;
        public TimeOfDaySpawnsDictionary timeOfDaySpawns = new TimeOfDaySpawnsDictionary();
    } // class AreaData
} // namespace Magique.SoulLink

using static Magique.SoulLink.SoulLinkSpawnerTypes;

// (c)2022 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    [System.Serializable]
    public class LayerRuleData
    {
        public FilterRule filterRule;
        public string layerName;
    } // class LayerRuleData
} // namespace Magique.SoulLink


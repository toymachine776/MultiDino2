﻿#if MIRROR
using Mirror;
#endif
#if SOULLINK_USE_NETCODE
using Unity.Netcode;
#endif
using UnityEngine;

// (c)2021-2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    [AddComponentMenu("SoulLink/Timed Despawn")]
    public class TimedDespawn : MonoBehaviour, ISpawnEvents, INetOwner
    {
        [SerializeField]
        private float _despwnTime = 0f;

        private bool _spawned = false;

        /// <summary>
        /// When using PoolManager, this function automatically gets called when the instance is spawned. Otherwise, it is not used.
        /// </summary>
        public void OnSpawned()
        {
            if (!IsOwner()) return;

            _spawned = true;
            CancelInvoke();

            // Invoke despawn function on timed value
            Invoke("Despawn", _despwnTime);
        } // OnSpawned()

        virtual public void OnDespawned()
        {
            // do nothing
        } // OnDespawned()

        private void Start()
        {
            if (!IsOwner()) return;

            if (!_spawned)
            {
                // Invoke despawn function on timed value
                Invoke("Despawn", _despwnTime);
            } // if
        } // Start()

        /// <summary>
        /// Force the spawn to despawn
        /// </summary>
        private void Despawn()
        {
            if (!IsOwner()) return;

            CancelInvoke();

            if (_spawned)
            {
                var spawnable = GetComponent<ISpawnable>();
                if (spawnable != null && isActiveAndEnabled)
                {
                    SoulLinkSpawner.Instance.Despawn(spawnable.GetGameObject().transform);
                }
                else
                {
                    SoulLinkSpawner.Instance.Despawn(transform);
                }
                return;
            } // if

            // Just destroy this if no pooling solution is being used
            Destroy(gameObject);
        } // Despawn()

        #region INetOwner
        public void SetIsOwner(bool isOwner)
        {
            // do nothing
        } // SetIsOwner()

        public bool IsOwner()
        {
#if MIRROR
            return (SoulLinkSpawner.IsMultiplayer && NetworkServer.active) || (!SoulLinkSpawner.IsMultiplayer);
#elif SOULLINK_USE_NETCODE
            return (SoulLinkSpawner.IsMultiplayer && NetworkManager.Singleton.IsServer) || (!SoulLinkSpawner.IsMultiplayer);
#else

#endif
            return true;
        } // IsOwner()
        #endregion
    } // class TimedDespawn
} // namespace Magique.SoulLink
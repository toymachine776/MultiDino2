﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// (c)2022 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    [System.Serializable]
    public class WaveInfoData
    {
        public List<WaveItemData> waveItems = new List<WaveItemData>();
    } // WaveInfoData
} // namespace Magique.SoulLink

#if MIRROR
using Mirror;
#endif
#if SOULLINK_USE_NETCODE
using Unity.Netcode;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

// (c)2022-2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    [AddComponentMenu("SoulLink/Assign Player")]
    public class AssignPlayer : MonoBehaviour
    {
        /// <summary>
        /// Assign the player's transform to SoulLinkSpawner when the player starts in the scene
        /// Update reference if a new scene is loaded, which may have a different SoulLinkSpawner component.
        /// </summary>
        void Start()
        {
            SetPlayerReference();
            SceneManager.sceneLoaded += OnSceneLoaded;
        } // Start()

        void SetPlayerReference()
        {
            if (SoulLinkSpawner.Instance != null)
            {
#if MIRROR
                if (GetComponent<NetworkIdentity>().isOwned)
                {
                    SoulLinkSpawner.Instance.RegisterPlayer(gameObject.transform);
                }
#elif SOULLINK_USE_NETCODE
                if (GetComponent<NetworkObject>().IsOwner)
                {
                    SoulLinkSpawner.Instance.RegisterPlayer(gameObject.transform);
                }
#else
                SoulLinkSpawner.Instance.RegisterPlayer(gameObject.transform);
#endif
            }
            else
            {
                Debug.LogWarning("AssignPlayer: SoulLink is not present in the scene. You may ignore this if this is your intention.");
            }
        } // SetPlayerReference()

        public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            SetPlayerReference();
        } // OnSceneLoaded()
    } // class AssignPlayer
} // namespace Magique.SoulLink

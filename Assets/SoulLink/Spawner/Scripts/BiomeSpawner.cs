using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Magique.SoulLink.SoulLinkSpawnerTypes;

// (c)2021-2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    public class BiomeSpawner : MonoBehaviour
    {
        public SoulLinkSpawnerDatabase Database
        {
            get { return _SoulLinkSpawnerDatabase; }
            set { _SoulLinkSpawnerDatabase = value; }
        }

        [SerializeField]
        private SoulLinkSpawnerDatabase _SoulLinkSpawnerDatabase;

        public Transform PlayerTransform
        {
            get { return _playerTransform; }
            set { _playerTransform = value; }
        }

        [SerializeField]
        private Transform _playerTransform;

        // Pool of random values for determining spawns from their probability
        public RandomValuePool RandomValues = new RandomValuePool();

        private int _iter = 0; // which iteration are we on
        private SoulLinkSpawner _S;

        // Player specific spawning data
        public Dictionary<Transform,int> Populations
        { get { return _populations; } }
        private Dictionary<Transform, int> _populations = new Dictionary<Transform, int>();

        /// <summary>
        ///  Awake
        /// </summary>
        private void Awake()
        {
            // Shortcut reference to the SoulLinkSpawner instance
            _S = SoulLinkSpawner.Instance;

            // Generate the random values required for generating spawns by probability
            RandomValues.GenerateValues((uint)_S.MaxSpawns);
        } // Awake()

        /// <summary>
        /// PopulationReached
        /// </summary>
        /// <param name="spawnData"></param>
        /// <returns></returns>
        public bool PopulationReached(SpawnData spawnData)
        {
            bool result = false;
            if (_populations.ContainsKey(spawnData.spawnPrefab))
            {
                result = (_populations[spawnData.spawnPrefab] == spawnData.populationCap);
            } // if

            return result;
        } // PopulationReached()

        public bool PopulationHas(Transform prefab)
        {
            return _populations.ContainsKey(prefab);
        } // PopulationHas();

        public bool CanSpawn(SpawnInfoData spawnInfoData)
        {
            return (_populations[spawnInfoData.prefab] < spawnInfoData.populationCap);
        } // CanSpawn()

        /// <summary>
        /// Increment the current population for the specified prefab
        /// </summary>
        /// <param name="sourceObj"></param>
        /// <param name="category"></param>
        public void PopulationAdd(Transform sourceObj, string category)
        {
            lock (_populations)
            {
                if (_populations.ContainsKey(sourceObj))
                {
                    _populations[sourceObj]++;
                }
                else
                {
                    _populations[sourceObj] = 1;
                }

                _S.IncTotalSpawns(category);
            } // lock
        } // PopulationAdd()

        /// <summary>
        /// Decrement the current population for the specified prefab
        /// </summary>
        /// <param name="sourceObj"></param>
        /// <param name="spawnCategory"></param>
        /// <returns></returns>
        public bool PopulationSubtract(Transform sourceObj, string spawnCategory)
        {
            if (sourceObj == null) return false;

            lock (_populations)
            {
                // Remove from population
                if (_populations.ContainsKey(sourceObj))
                {
                    _populations[sourceObj]--;
                    _S.DecTotalSpawns(spawnCategory);

                    if (_populations[sourceObj] == 0)
                    {
                        _populations.Remove(sourceObj);
                        return true;
                    } // if
                } // if
            } // lock

            return false;
        } // PopulationSubtract()

        /// <summary>
        /// Get the current population for the specified prefab.
        /// </summary>
        /// <param name="sourceObj"></param>
        /// <returns></returns>
        public int GetPopulation(Transform sourceObj)
        {
            if (_populations.ContainsKey(sourceObj))
            {
                return _populations[sourceObj];
            } // if

            return 0;
        } // GetPopulation()

        /// <summary>
        /// Get the sum of all populations for all prefabs
        /// </summary>
        /// <returns></returns>
        public int GetTotalPopulations()
        {
            int total = 0;
            foreach (var population in _populations)
            {
                total += population.Value;
            } // foreach

            return total;
        } // GetTotalPopulations()

        /// <summary>
        ///  SwitchDatabase
        /// </summary>
        /// <param name="database"></param>
        public void SwitchDatabase(SoulLinkSpawnerDatabase database)
        {
            // Stop GenerateSpawns co-routine
            StopSpawning();

            // Change database reference
            _SoulLinkSpawnerDatabase = database;

            // Restart Generate Spawns co-routine
            StartSpawning();
        } // SwitchDatabase()

        public void StartSpawning()
        {
            StartCoroutine(GenerateSpawns());
        } // StartSpawning()

        public void StopSpawning()
        {
            StopCoroutine(GenerateSpawns());
        } // StopSpawning()

        /// <summary>
        /// GenerateSpawns
        /// </summary>
        /// <returns></returns>
        IEnumerator GenerateSpawns()
        {
            yield return new WaitForSeconds(_S.DelaySpawnsOnStart);

            float spawnRange = _S.SpawnRadius - _S.MinimumSpawnRange;
            float incrementalRange = (spawnRange / _S.SpawnCircles);
            int iter = 0; // which iteration are we on

            while (!_S.Paused && true && Database != null)
            {
                // Skip if Biomes spawning is disabled or there is no player transform reference
                if (_S.DisableBiomeSpawns || PlayerTransform == null)
                {
                    yield return null;
                    continue;
                } // if

                yield return new WaitForSeconds(1f / _S.SpawnRate);

                var totalSpawns = _S.TotalSpawns;
                if (totalSpawns < _S.MaxSpawns) // Checks against total spawns for all categories
                {
                    // Pick a random spawn position
                    var minRange = _S.UseSpawnCircles ? (_S.MinimumSpawnRange + (iter * incrementalRange)) : _S.MinimumSpawnRange;
                    var maxRange = _S.UseSpawnCircles ? (minRange + incrementalRange) : _S.SpawnRadius;

                    var spawnPos = _S.GetSpawnPos(minRange, maxRange, PlayerTransform);
                    ++iter;

                    if (iter == _S.SpawnCircles)
                    {
                        iter = 0;
                    } // if

                    // First reduce the possible spawns by querying only biome spawn data within current time of day definitions and
                    // the biomes that meet the threshold requirements. Also checks for whether it has been disabled.
                    var query =
                        from biomeData in Database.biomeSpawnData
                        where _S.CurrentBiomes.Contains(biomeData.biomeName) && !biomeData.disabled && biomeData.timeOfDaySpawns.Any(i => _S.CurrentTOD.Contains(i.Key))
                        select biomeData;

                    // If no time of day definitions match the current time of day then log it so that the user can debug the issue
                    if (_S.CurrentTOD.Count == 0)
                    {
                        SoulLinkGlobal.DebugLog(gameObject, "No Time of Day Definitions qualify for spawns. Nothing will spawn.");
                    } // if

                    bool viableSpawn = false;

                    foreach (var item in query)
                    {
                        // Pick a random entry from available time of days
                        var tod = _S.RandomCurrentTimeOfDay();

                        if (item.timeOfDaySpawns.ContainsKey(tod))
                        {
                            if (item.timeOfDaySpawns[tod].spawnInfoData.Count == 0) continue;

                            // Pick a random entry from available spawns
                            var index = UnityEngine.Random.Range(0, item.timeOfDaySpawns[tod].spawnInfoData.Count);
                            var spawnInfoData = item.timeOfDaySpawns[tod].spawnInfoData[index];
                            if (spawnInfoData.disable) continue;

                            spawnInfoData.randomChance = 0;
                            spawnInfoData.herdID = 0;

                            // Set a flag indicating that there was at least one valid spawn that could be chosen from the current query
                            // if by the end of the foreach loop, this flag was not set then log something to inform the user
                            viableSpawn = true;

                            // Check for a custom spawn validator and call its PreValidateSpawn method if it exists. This is used for rejecting a spawn as early as
                            // possible. Returning true will have no affect because further filters/rules can still reject it later.
                            bool canSpawn = true;
                            if (_S.SpawnValidator != null)
                            {
                                canSpawn = _S.SpawnValidator.PreValidateSpawn(spawnInfoData);
                            } // if

                            if (!canSpawn) continue;

                            // Check if there are any spawns of any type within a certain range. If so then skip it
                            float proximityRange = ((_S.SpawnRadius * _S.ClusterRange) / spawnInfoData.populationCap);
                            if (_S.PoolManager.AnySpawnInRange(spawnPos, proximityRange)) continue;

                            short chance = 0;
                            canSpawn = (spawnInfoData.probability > 0 && RandomValues.FindValue((short)spawnInfoData.probability, out chance) &&
                                _S.CanSpawn(spawnInfoData.spawnCategory));
                            if (!canSpawn)
                            {
                                canSpawn = (_S.SpawnValidator != null ? _S.SpawnValidator.PostValidateSpawn(spawnInfoData, SpawnValidationFailure.Probability) : canSpawn);
                            } // if

                            if (!canSpawn)
                            {
                                FailSpawn(spawnInfoData, "random chance exceeded probability.");
                                continue;
                            } // if

                            // Check if a herd can spawn at least the minimum qty before allowing it
                            if (spawnInfoData.spawnType == SpawnType.Herd && (_S.TotalSpawns + spawnInfoData.herd.minPopulation) > _S.MaxSpawns)
                            {
                                FailSpawn(spawnInfoData, "spawning the herd would exceed max spawns");
                                continue;
                            } // if

                            if (canSpawn)
                            {
                                // TODO: Solve the issue of herds spawning too close to other herds of the same type. Possibly use HerdData to insure
                                //  that the herd object is up to date by re-calculating a sphere volume as its members move about
                                if (spawnInfoData.spawnType == SpawnType.Herd)
                                {
                                    uint herdID = SoulLinkSpawner.Instance.AddHerd(chance);

                                    // Spawn a randomized qty
                                    int qtyDesired = UnityEngine.Random.Range(spawnInfoData.herd.minPopulation, spawnInfoData.herd.maxPopulation + 1);
                                    int qty = 0;
                                    for (int i = 0; i < qtyDesired; ++i)
                                    {
                                        float herdMinElevation = spawnInfoData.overrideElevation ? spawnInfoData.minElevation : _S.MinElevation;
                                        float herdMaxElevation = spawnInfoData.overrideElevation ? spawnInfoData.maxElevation : _S.MaxElevation;
                                        float herdMinSlopeAngle = spawnInfoData.overrideSlopeAngle ? spawnInfoData.minSlopeAngle : _S.MinSlopeAngle;
                                        float herdMaxSlopeAngle = spawnInfoData.overrideSlopeAngle ? spawnInfoData.maxSlopeAngle : _S.MaxSlopeAngle;

                                        foreach (var spawn in spawnInfoData.herd.herdPrefabs)
                                        {
                                            // Randomize position in radius around spawnPos
                                            Vector3 newPos = Utility.GetRandomPos(0f, spawnInfoData.herd.spawnRadius, spawnPos, _S.SpawnMode, _S.SpawnAxes);
                                            Vector3 herdSpawnPos = new Vector3(newPos.x, _S.GetYPos(newPos), newPos.z);

                                            float minElevation = spawn.overrideElevation ? spawn.minElevation : herdMinElevation;
                                            float maxElevation = spawn.overrideElevation ? spawn.maxElevation : herdMaxElevation;
                                            float minSlopeAngle = spawn.overrideSlopeAngle ? spawn.minSlopeAngle : herdMinSlopeAngle;
                                            float maxSlopeAngle = spawn.overrideSlopeAngle ? spawn.maxSlopeAngle : herdMaxSlopeAngle;

                                            // Assign the herd's filters to the individual spawns to validate them properly
                                            spawn.textureRules = spawnInfoData.textureRules;
                                            spawn.weatherRules = spawnInfoData.weatherRules;
                                            spawn.seasonRules = spawnInfoData.seasonRules;
                                            spawn.questRules = spawnInfoData.questRules;
                                            spawn.proximityRules = spawnInfoData.proximityRules;
                                            spawn.spawnAreaGuid = string.Empty;
                                            spawn.waveSpawnerGuid = string.Empty;
                                            spawn.waveItemGuid = string.Empty;

                                            short herdSpawnChance = (short)UnityEngine.Random.Range(1, 101);
                                            if (spawn.probability > 0 && herdSpawnChance < (uint)spawn.probability)
                                            {
                                                spawn.randomChance = 0;
                                                spawn.herdID = herdID;

                                                if (_S.ValidateSpawnRules(_playerTransform, spawn, herdSpawnPos, tod, spawn.ignorePlayerFOV, 
                                                    minElevation, maxElevation, minSlopeAngle, maxSlopeAngle))
                                                {
                                                    qty++;
                                                } // if
                                            } // if

                                            if (qty == qtyDesired) break;
                                        } // foreach

                                        if (qty == qtyDesired) break;

                                        yield return null;
                                    } // for i

                                    // If we spawned any instances from the herd, generate proximity spawns as necessary
                                    if (qty > 0 && Database != null)
                                    {
                                        _S.GenerateProximitySpawns(_playerTransform, Database.GetProximitySpawns(spawnInfoData.spawnGuid), tod, spawnPos);
                                    } // if
                                }
                                else
                                {
                                    if (spawnInfoData.spawnType == SpawnType.Variants && spawnInfoData.prefabVariants.Count > 0)
                                    {
                                        spawnInfoData.prefab = spawnInfoData.prefabVariants[UnityEngine.Random.Range(0, spawnInfoData.prefabVariants.Count)];
                                    } // if

                                    float minElevation = spawnInfoData.overrideElevation ? spawnInfoData.minElevation : _S.MinElevation;
                                    float maxElevation = spawnInfoData.overrideElevation ? spawnInfoData.maxElevation : _S.MaxElevation;
                                    float minSlopeAngle = spawnInfoData.overrideSlopeAngle ? spawnInfoData.minSlopeAngle : _S.MinSlopeAngle;
                                    float maxSlopeAngle = spawnInfoData.overrideSlopeAngle ? spawnInfoData.maxSlopeAngle : _S.MaxSlopeAngle;

                                    spawnInfoData.spawnAreaGuid = string.Empty;
                                    spawnInfoData.waveSpawnerGuid = string.Empty;
                                    spawnInfoData.waveItemGuid = string.Empty;
                                    spawnInfoData.randomChance = chance;
                                    _S.ValidateSpawnRules(_playerTransform, spawnInfoData, spawnPos, tod, spawnInfoData.ignorePlayerFOV, 
                                        minElevation, maxElevation, minSlopeAngle, maxSlopeAngle); ;
                                }
                            } // if
                        } // if tod in set
                        else
                        {
                            SoulLinkGlobal.DebugLog(gameObject, "No spawns for the selected time of day. Selected Time of Day = " + tod);
                        }
                    } // foreach item

                    if (!viableSpawn)
                    {
                        SoulLinkGlobal.DebugLog(gameObject, "No spawns for the available biomes for the proposed spawn position.");
                    } // if
                } // if
            } // while
        } // GenerateSpawns()

        /// <summary>
        /// Call this method to report spawn failure and return the random chance value to the pool
        /// </summary>
        /// <param name="spawnInfoData"></param>
        /// <param name="failureText"></param>
        public void FailSpawn(SpawnInfoData spawnInfoData, string failureText)
        {
            var spawnName = (spawnInfoData.prefab != null) ? spawnInfoData.prefab.name : "<null>";
            SoulLinkGlobal.DebugLog(gameObject, "Failed to spawn " + spawnName + " " + failureText);
            RandomValues.ReturnValue(spawnInfoData.randomChance);
        } // FailSpawn()

        /// <summary>
        /// Call this method to report spawn failure and return the random chance value to the pool
        /// </summary>
        /// <param name="spawnInfoData"></param>
        /// <param name="failureText"></param>
        public void FailSpawn(SpawnData spawnData, string failureText)
        {
            var spawnName = (spawnData.spawnPrefab != null) ? spawnData.spawnPrefab.name : "<null>";
            SoulLinkGlobal.DebugLog(gameObject, "Failed to spawn " + spawnName + " " + failureText);
            RandomValues.ReturnValue(spawnData.randomChance);
        } // FailSpawn()

    } // class BiomeSpawner
} //namespace Magique.SoulLink

2D SHMUP

This demo is a fully working 2D shoot 'em up game to demonstrate the Advanced Wave Spawner and 2D spawning features of SoulLink Spawner.
All art and sound assets were taken from the public domain or created specifically for this demo and can be used by you for any purposes 
without credit.

All scripts are examples to demonstrate a simple shmup style game. They are not intended for production use, but you may use them as
you see fit. All scripts are Copyright 2022-2023 Magique Productions, Ltd.
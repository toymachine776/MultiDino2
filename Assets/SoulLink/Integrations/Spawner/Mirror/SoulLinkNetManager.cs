#if MIRROR
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// SoulLinkNetManager
// An example network manager to demonstrate how to regisetr and unregister players to SoulLinkSpawner

// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    public class SoulLinkNetManager : NetworkManager
    {
        [Serializable] 
        public class UnityEventServerAddPlayer : UnityEvent<NetworkConnectionToClient> { }

        [Serializable] 
        public class UnityEventServerDisconnect : UnityEvent<NetworkConnectionToClient> { }

        [Header("Events")]
        public UnityEventServerAddPlayer onServerAddPlayer;
        public UnityEventServerDisconnect onServerDisconnect;

        /// <summary>
        /// Called on the server when a client adds a new player with NetworkClient.AddPlayer.
        /// <para>The default implementation for this function creates a new player object from the playerPrefab.</para>
        /// </summary>
        /// <param name="conn">Connection from client.</param>
        public override void OnServerAddPlayer(NetworkConnectionToClient conn)
        {
            base.OnServerAddPlayer(conn);
            onServerAddPlayer?.Invoke(conn);
        } // OnServerAddPlayer()

        /// <summary>
        /// Called on the server when a client disconnects.
        /// <para>This is called on the Server when a Client disconnects from the Server. Use an override to decide what should happen when a disconnection is detected.</para>
        /// </summary>
        /// <param name="conn">Connection from client.</param>
        public override void OnServerDisconnect(NetworkConnectionToClient conn)
        {
            base.OnServerDisconnect(conn);
            onServerDisconnect?.Invoke(conn);
        } // OnServerDisconnect()
    } // class SoulLinkNetManager
} // namespace Magique.SoulLink
#endif

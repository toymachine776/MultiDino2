#if MIRROR
using Mirror;
using UnityEngine;

// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class GameTimeSync : NetworkBehaviour
    {
        [SyncVar]
        private int day;

        [SyncVar]
        private int month;

        [SyncVar]
        private float hour;

        [SyncVar]
        private float minute;

        private void Start()
        {
            GetComponent<NetworkBehaviour>().syncInterval = SoulLinkSpawner.Instance.TimeSyncFrequency;
        } // Start()

        void Update()
        {
            if (isServer)
            {
                day = SoulLinkSpawner.Instance.GameTime.GetDay();
                month = SoulLinkSpawner.Instance.GameTime.GetMonth();
                hour = SoulLinkSpawner.Instance.GameTime.GetHour();
                minute = SoulLinkSpawner.Instance.GameTime.GetMinute();
            }
            else
            {
                SoulLinkSpawner.Instance.GameTime.SetDay(day);
                SoulLinkSpawner.Instance.GameTime.SetMonth(month);
                SoulLinkSpawner.Instance.GameTime.SetHour(hour);
                SoulLinkSpawner.Instance.GameTime.SetMinute(minute);
            }
        } // Update()
    } // class GameTimeSync
} // namespace Magique.SoulLink
#endif

#if MIRROR
using Mirror;
using Mirror.Examples.Tanks;
using UnityEngine;

// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    public class PoolManager_Mirror : PoolManager_SoulLink, INetOwner
    {
        GameObject SpawnHandler(SpawnMessage msg) => Spawn(msg);
        void UnspawnHandler(GameObject spawned) => Despawn(spawned.transform);

        public override void Start()
        {
            base.Start();

            foreach (var item in _objectPools)
            {
                if (item.Key != null)
                {
                    if (item.Key.GetComponent<NetworkIdentity>() != null)
                    {
                        NetworkClient.RegisterPrefab(item.Key.gameObject, SpawnHandler, UnspawnHandler);
                    } // if
                }
            } // foreach
        } // Start()

        /// Spawn through Mirror
        /// </summary>
        /// <param name="msg"></param>
        public GameObject Spawn(SpawnMessage msg)
        {
            // get the prefab to spawn from the msg.netId
            foreach (var pool in _objectPools)
            {
                if (pool.Value.Prefab.GetComponent<NetworkIdentity>().assetId == msg.assetId)
                {
                    // TODO: how can we use the parentTransform parameter?
                    var spawn = Spawn(pool.Value.Prefab, msg.position, msg.rotation, null, msg.scale.x, msg.scale.x, pool.Value.Prefab.localScale,
                        IsOwner());
                    if (spawn != null)
                    {
                        if (!IsOwner())
                        {
                            Debug.LogWarning("   successful spawn returned");
                        }
                        return spawn.gameObject;
                    } // if
                } // if
            } // foreach

            return null;
        } // Spawn()


        public override Transform Spawn(Transform itemPrefab, Vector3 position, Quaternion rotation, Transform parentTransform, 
            float minScale, float maxScale, Vector3 origScale, bool isOwner = false)
        {
            var spawn = base.Spawn(itemPrefab, position, rotation, parentTransform, minScale, maxScale, origScale, IsOwner());

            if (IsOwner() && spawn != null)
            {
                NetworkServer.Spawn(spawn.gameObject);
            } // if

            return spawn;
        } // Spawn()

        public override bool Despawn(Transform instance)
        {
            return base.Despawn(instance);
        } // Despawn()

        override public void Reclaim(Transform instance)
        {
            base.Reclaim(instance);

            if (IsOwner())
            {
                NetworkServer.UnSpawn(instance.gameObject);
            } // if
        } // if

        /// <summary>
        /// Unregister all prefabs from NetworkClient
        /// </summary>
        private void OnDestroy()
        {
            foreach (var pool in _objectPools)
            {
                NetworkClient.UnregisterPrefab(pool.Value.Prefab.gameObject);
            } // foreach
        } // OnDestroy()

        #region INetOwner
        public void SetIsOwner(bool isOwner)
        {
            // do nothing
        } // SetIsOwner()

        public bool IsOwner()
        {
#if MIRROR
            return (SoulLinkSpawner.IsMultiplayer && NetworkServer.active) || (!SoulLinkSpawner.IsMultiplayer);
#else
            return true;
#endif
        } // IsOwner()

        public Transform GetPlayer()
        {
            throw new System.NotImplementedException();
        }

        public void SetPlayer(Transform player)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    } // PoolManager_Mirror
} // namespace Magique.SoulLink
#endif
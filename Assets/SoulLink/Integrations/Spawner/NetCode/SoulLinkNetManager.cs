#if SOULLINK_USE_NETCODE
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

// SoulLinkNetManager
// An example network manager to demonstrate how to regisetr and unregister players to SoulLinkSpawner

// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    public class SoulLinkNetManager : NetworkManager
    {
        [Serializable] 
        public class UnityEventClientConnected : UnityEvent<Transform> { }

        [Serializable] 
        public class UnityEventClientDisconnected : UnityEvent<Transform> { }

        [Header("Events")]
        public UnityEventClientConnected onClientConnected;
        public UnityEventClientDisconnected onClientDisconnected;

        private Dictionary<ulong, Transform> _clientObjects = new Dictionary<ulong, Transform>();

        private void Start()
        {
            OnClientConnectedCallback += OnClientConnected;
            OnClientDisconnectCallback += OnClientDisconnected;

            onClientConnected.AddListener(SoulLinkSpawner.Instance.RegisterPlayer);
            onClientDisconnected.AddListener(SoulLinkSpawner.Instance.UnregisterPlayer);
        } // Start()

        public void DoStartHost()
        {
            StartHost();
        } // DoStartHost()

        public void DoStartClient()
        {
            StartClient();
        } // DoStartClient()

        public void OnClientConnected(ulong clientId)
        {
            if (!IsServer) return;

            _clientObjects[clientId] = ConnectedClients[clientId].PlayerObject.gameObject.transform;
            onClientConnected?.Invoke(ConnectedClients[clientId].PlayerObject.gameObject.transform);
        } // OnServerAddPlayer()

        public void OnClientDisconnected(ulong clientId)
        {
            if (!IsServer) return;

            onClientDisconnected?.Invoke(_clientObjects[clientId]);
            _clientObjects.Remove(clientId);
        } // OnClientDisconnected()
    } // class SoulLinkNetManager
} // namespace Magique.SoulLink
#endif

#if SOULLINK_USE_NETCODE
using Unity.Netcode;
using UnityEngine;

// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    public class PoolManager_NetCode : PoolManager_SoulLink, INetOwner, INetworkPrefabInstanceHandler
    {
        public override void Start()
        {
            base.Start();

            foreach (var item in _objectPools)
            {
                if (item.Key != null)
                {
                    if (item.Key.GetComponent<NetworkObject>() != null)
                    {
                        NetworkManager.Singleton.PrefabHandler.AddHandler(item.Key.gameObject, this);

                        if (!NetworkManager.Singleton.NetworkConfig.Prefabs.Contains(item.Key.gameObject))
                        {
                            NetworkManager.Singleton.AddNetworkPrefab(item.Key.gameObject);
                        } // if
                    } // if
                }
            } // foreach
        } // Start()

        NetworkObject INetworkPrefabInstanceHandler.Instantiate(ulong ownerClientId, Vector3 position, Quaternion rotation)
        {
            // get the prefab to spawn from the ownerClientId
            foreach (var pool in _objectPools)
            {
                if (pool.Value.Prefab.GetComponent<NetworkObject>().NetworkObjectId == ownerClientId)
                {
                    // TODO: how can we use the parentTransform parameter?
                    var spawn = Spawn(pool.Value.Prefab, position, rotation, null, 1f, 1f, pool.Value.Prefab.localScale);
                    if (spawn != null)
                    {
                        if (!IsOwner())
                        {
                            Debug.LogWarning("   successful spawn returned");
                        }
                        return spawn.gameObject.GetComponent<NetworkObject>();
                    } // if
                } // if
            } // foreach

            return null;
        } // Instantiate()

        void INetworkPrefabInstanceHandler.Destroy(NetworkObject networkObject)
        {
            Despawn(networkObject.transform);
        } // Destroy()

        public override Transform Spawn(Transform itemPrefab, Vector3 position, Quaternion rotation, Transform parentTransform, float minScale, 
            float maxScale, Vector3 origScale, bool isOwner = false)
        {
            var spawn = base.Spawn(itemPrefab, position, rotation, parentTransform, minScale, maxScale, origScale, IsOwner());

            if (IsOwner() && spawn != null)
            {
                spawn.GetComponent<NetworkObject>().Spawn();
            } // if

            return spawn;
        } // Spawn()

        public override bool Despawn(Transform instance)
        {
            return base.Despawn(instance);
        } // Despawn()

        override public void Reclaim(Transform instance)
        {
            base.Reclaim(instance);

            if (IsOwner())
            {
                instance.GetComponent<NetworkObject>().Despawn();
            } // if
        } // if

        /// <summary>
        /// Unregister all prefabs from NetworkManager
        /// </summary>
        private void OnDestroy()
        {
            // TODO: this does not appear to be necessary since the prefabs are store din a scriptable object
            // Is there a betetr way to register these without modifying a permanent scriptable object?
//            foreach (var pool in _objectPools)
//            {
//                NetworkManager.Singleton.RemoveNetworkPrefab(pool.Value.Prefab.gameObject);
//            } // foreach
        } // OnDestroy()

        #region INetOwner
        public void SetIsOwner(bool isOwner)
        {
            // do nothing
        } // SetIsOwner()

        public bool IsOwner()
        {
#if SOULLINK_USE_NETCODE
            return (SoulLinkSpawner.IsMultiplayer && NetworkManager.Singleton.IsServer) || (!SoulLinkSpawner.IsMultiplayer);
#else
            return true;
#endif
        } // IsOwner()

        public Transform GetPlayer()
        {
            throw new System.NotImplementedException();
        }

        public void SetPlayer(Transform player)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    } // PoolManager_NetCode
} // namespace Magique.SoulLink
#endif
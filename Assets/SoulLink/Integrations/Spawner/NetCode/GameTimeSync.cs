#if SOULLINK_USE_NETCODE
using Unity.Netcode;
using UnityEngine;

// (c)2023 Magique Productions, Ltd. All rights reserved worldwide.

namespace Magique.SoulLink
{
    [RequireComponent(typeof(NetworkObject))]
    public class GameTimeSync : NetworkBehaviour
    {
        private NetworkVariable<int> day = new NetworkVariable<int>();
        private NetworkVariable<int> month = new NetworkVariable<int>();
        private NetworkVariable<float> hour = new NetworkVariable<float>();
        private NetworkVariable<float> minute = new NetworkVariable<float>();

        private void Start()
        {
            day.OnValueChanged += OnDayChanged;
            month.OnValueChanged += OnMonthChanged;
            hour.OnValueChanged += OnHourChanged;
            minute.OnValueChanged += OnMinuteChanged;
        } // Start()

        void Update()
        {
            if (IsServer)
            {
                day.Value = SoulLinkSpawner.Instance.GameTime.GetDay();
                month.Value = SoulLinkSpawner.Instance.GameTime.GetMonth();
                hour.Value = SoulLinkSpawner.Instance.GameTime.GetHour();
                minute.Value = SoulLinkSpawner.Instance.GameTime.GetMinute();
            } // if
        } // Update()

        /// <summary>
        /// Synchronize day when the value has changed
        /// </summary>
        /// <param name="preDay"></param>
        /// <param name="newDay"></param>
        void OnDayChanged(int preDay, int newDay)
        {
            SoulLinkSpawner.Instance.GameTime.SetDay(newDay);
        } // OnDayChanged()

        /// <summary>
        /// Synchronize month when the value has changed
        /// </summary>
        /// <param name="prevMonth"></param>
        /// <param name="newMonth"></param>
        void OnMonthChanged(int prevMonth, int newMonth)
        {
            SoulLinkSpawner.Instance.GameTime.SetMonth(newMonth);
        } // OnMonthChanged()

        /// <summary>
        /// Synchronize hour when the value has changed
        /// </summary>
        /// <param name="prevHour"></param>
        /// <param name="newHour"></param>
        void OnHourChanged(float prevHour, float newHour)
        {
            SoulLinkSpawner.Instance.GameTime.SetHour(newHour);
        } // OnHourChanged()

        /// <summary>
        /// Synchronize minute when the value has changed
        /// </summary>
        /// <param name="prevMinute"></param>
        /// <param name="newMinute"></param>
        void OnMinuteChanged(float prevMinute, float newMinute)
        {
            SoulLinkSpawner.Instance.GameTime.SetMinute(newMinute);
        } // OnMinuteChanged()
    } // class GameTimeSync
} // namespace Magique.SoulLink
#endif

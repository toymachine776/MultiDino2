#if COZY_3_AND_UP

// CozyGameTime
// An integration with COZY: Stylized Weather 3 System by Distant Lands. 
//
// To use with Dialogue System or Quest Machine by Pixel Crushers, attach a GameTime_Saver component to the same object that this 
// component is attached to.
//
// (c)2024 Magique Productions, Ltd. All rights reserved worldwide.

using DistantLands.Cozy;
using UnityEngine;

namespace Magique.SoulLink
{
    public class Cozy3GameTime : BaseGameTime
    {
        private CozyWeather _sky;

        override public void GetComponents()
        {
            if (_sky != null) return;

            _sky = CozyWeather.instance;
            if (_sky)
            {
                _sky.perennialProfile.pauseTime = false;

                _currentHour = _sky.timeModule.currentTime.hours;
                _currentMinute = _sky.timeModule.currentTime.minutes;

                _dayLengthInMinutes = CozyWeather.instance.timeModule.modifiedDayPercentage * 1440;
                SetDayLength(_dayLengthInMinutes);
            } // if
        } // GetComponents()

        public override float GetHour()
        {
            if (!_sky) return base.GetHour();

            _currentHour = _sky.timeModule.currentTime.hours;
            return _currentHour;
        } // GetHour()

        override public void SetHour(float hour)
        {
            SetCozyTime(hour);
            _currentHour = (int)hour;
        } // SetHour()

        override public void SetMinute(float minute)
        {
            SetCozyTime(_currentHour + (minute / 60));
            _currentMinute = (int)minute;
        } // SetMinute()

        public override float GetMinute()
        {
            if (!_sky) return base.GetMinute();

            _currentMinute = _sky.timeModule.currentTime.minutes;
            return _currentMinute;
        } // GetMinute()

        void SetCozyTime(float time)
        {
            CozyWeather.instance.timeModule.currentTime = time;// currentTicks = (time / 24f) * (CozyWeather.instance.perennialProfile.ticksPerDay);
        } // SetCozyTime

        override public int GetDay()
        {
            GetComponents();

            if (!_sky) return 0;

            _currentDay = _sky.timeModule.currentDay;
            return _currentDay;
        } // GetDay()

        public override void SetDay(int value)
        {
            if (!_sky) return;

            _sky.timeModule.currentDay = value;
        } // SetDay()

        public override int GetMonth()
        {
            GetComponents();

            if (!_sky) return 0;

            _currentMonth = Mathf.FloorToInt(CozyWeather.instance.timeModule.GetCurrentYearPercentage() * 12) + 1;
            return _currentMonth;
        } // GetMonth()

        public override void SetMonth(int value)
        {
            if (!_sky) return;

            CozyWeather.instance.timeModule.currentDay = value * (CozyWeather.instance.perennialProfile.daysPerYear / 12);
            _currentMonth = value;
        } // SetMonth()

        public override int GetYear()
        {
            GetComponents();

            if (!_sky) return 0;

            _currentYear = _sky.timeModule.currentYear;
            return _currentYear;
        } // GetYear()

        public override void SetYear(int value)
        {
            if (!_sky) return;

            _currentYear = value;
            _sky.timeModule.currentYear = value;
        } // SetYear()

        override public void Pause()
        {
            _paused = true;

            if (!_sky) return;

            _sky.perennialProfile.pauseTime = true;
        } // Pause()

        public override void UnPause()
        {
            _paused = false;

            if (!_sky) return;

            _sky.perennialProfile.pauseTime = false;
        } // UnPause()

        override protected void FixedUpdate()
        {
            UpdateSecondsAdded();
        } // FixedUpdate()
    } // class Cozy3GameTime
} // namespace Magique.SoulLink
#endif
